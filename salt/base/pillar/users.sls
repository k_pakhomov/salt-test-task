---
users-formula:
  lookup:
    root_group: root

groups:
  groupA:
    state: present
  groupB:
    state: present
  groupC:
    state: present

users:
  userA:
    fullname: UserA
  userB:
    fullname: UserB
  userC:
    fullname: UserC

absent_groups:
  - group1
  - group2
  - group3

absent_users:
  - user1
  - user2
  - user3
