# vim: sts=2 ts=2 sw=2 et ai
{% from "users/map.jinja" import users with context %}

{% for group, setting in salt['pillar.get']('groups', {}).items() %}
{%   if setting.absent is defined and setting.absent or setting.get('state', "present") == 'absent' %}
users_group_absent_{{ group }}:
  group.absent:
    - name: {{ group }}
{% else %}
users_group_present_{{ group }}:
  group.present:
    - name: {{ group }}
{% endif %}
{% endfor %}

{% for name, user in pillar.get('users', {}).items()
        if user.absent is defined and user.absent %}
users_absent_user_{{ name }}:
{% if 'purge' in user or 'force' in user %}
  user.absent:
    - name: {{ name }}
    {% if 'purge' in user %}
    - purge: {{ user['purge'] }}
    {% endif %}
    {% if 'force' in user %}
    - force: {{ user['force'] }}
    {% endif %}
{% else %}
  user.absent:
    - name: {{ name }}
{% endif -%}
{% endfor %}

{% for name, user in pillar.get('users', {}).items() %}
{%- set current = salt.user.info(name) -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set createhome = user.get('createhome', users.get('createhome')) -%}

users_{{ name }}_user:
  user.present:
    - name: {{ name }}
    - home: {{ home }}
    - shell: {{ user.get('shell', "/bin/bash") }}
    {% if 'uid' in user -%}
    - uid: {{ user['uid'] }}
    {% endif -%}
    {% if 'password' in user -%}
    - password: '{{ user['password'] }}'
    {% endif -%}
    {% if user.get('empty_password') -%}
    - empty_password: {{ user.get('empty_password') }}
    {% endif -%}
    {% if 'enforce_password' in user -%}
    - enforce_password: {{ user['enforce_password'] }}
    {% endif -%}
    {% if 'hash_password' in user -%}
    - hash_password: {{ user['hash_password'] }}
    {% endif -%}
    {% if user.get('system', False) -%}
    - system: True
    {% endif -%}
    {% if 'fullname' in user %}
    - fullname: {{ user['fullname'] }}
    {% endif -%}
    {% if 'roomnumber' in user %}
    - roomnumber: {{ user['roomnumber'] }}
    {% endif %}
    {% if 'workphone' in user %}
    - workphone: {{ user['workphone'] }}
    {% endif %}
    {% if 'homephone' in user %}
    - homephone: {{ user['homephone'] }}
    {% endif %}
    - createhome: {{ createhome }}
    {% if not user.get('unique', True) %}
    - unique: False
    {% endif %}
    {% if 'expire' in user -%}
        {% if grains['kernel'].endswith('BSD') and
            user['expire'] < 157766400 %}
        {# 157762800s since epoch equals 01 Jan 1975 00:00:00 UTC #}
    - expire: {{ user['expire'] * 86400 }}
        {% elif grains['kernel'] == 'Linux' and
            user['expire'] > 84006 %}
        {# 2932896 days since epoch equals 9999-12-31 #}
    - expire: {{ (user['expire'] / 86400) | int }}
        {% else %}
    - expire: {{ user['expire'] }}
        {% endif %}
    {% endif -%}
    {% if 'mindays' in user %}
    - mindays: {{ user.get('mindays', None) }}
    {% endif %}
    {% if 'maxdays' in user %}
    - maxdays: {{ user.get('maxdays', None) }}
    {% endif %}
    {% if 'inactdays' in user %}
    - inactdays: {{ user.get('inactdays', None) }}
    {% endif %}
    {% if 'warndays' in user %}
    - warndays: {{ user.get('warndays', None) }}
    {% endif %}
    - remove_groups: {{ user.get('remove_groups', 'False') }}
    - groups:
      {% for group in user.get('groups', []) -%}
      - {{ group }}
      {% endfor %}
    {% if 'optional_groups' in user %}
    - optional_groups:
      {% for optional_group in user['optional_groups'] -%}
      - {{ optional_group }}
      {% endfor %}
    {% endif %}
{% endfor %}


{% for user in pillar.get('absent_users', []) %}
users_absent_user_2_{{ user }}:
  user.absent:
    - name: {{ user }}
{% endfor %}

{% for group in pillar.get('absent_groups', []) %}
users_absent_group_{{ group }}:
  group.absent:
    - name: {{ group }}
{% endfor %}
